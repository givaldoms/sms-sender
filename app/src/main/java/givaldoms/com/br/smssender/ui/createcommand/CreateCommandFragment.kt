package givaldoms.com.br.smssender.ui.createcommand

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import givaldoms.com.br.smssender.R
import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.data.source.interactor.CommandInteractorImpl
import givaldoms.com.br.smssender.data.source.local.AppDatabase
import givaldoms.com.br.smssender.extension.clear
import kotlinx.android.synthetic.main.fragment_add_command.*
import org.jetbrains.anko.support.v4.onUiThread
import org.jetbrains.anko.support.v4.toast
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions


class CreateCommandFragment : Fragment(), EasyPermissions.PermissionCallbacks, CreateCommandInterface.View {

    companion object {
        private const val STORAGE_PERMISSION = 122
    }

    private var mListener: OnFragmentInteractionListener? = null

    private lateinit var presenter: CreateCommandInterface.Presenter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_command, container, false)
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context?.toString() + " must implement OnFragmentInteractionListener")
        }

        presenter = CreateCommandPresenterImpl(CommandInteractorImpl(
                AppDatabase.getInstance(mListener as AppCompatActivity).CommandDao()),
                this)

    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onStart() {
        super.onStart()

        importCommandButton.setOnClickListener {
            requestStoragePermission()
        }

        addCommandToolbar.setNavigationOnClickListener {
            (mListener as AppCompatActivity).onBackPressed()
        }

        cancelAddCommandButton.setOnClickListener {
            mListener?.onCancelClick()
        }

        saveCommandButton.setOnClickListener {
            val title = commandTitleInputEditText.text.toString()
            val message = commandMessageInputEditText.text.toString()
            commandTitleInputEditText.clear()
            commandMessageInputEditText.clear()

            val c = Command(title, message)

            presenter.addCommand(c)

            toast("Comando adicionado com sucesso")

        }

    }

    @AfterPermissionGranted(STORAGE_PERMISSION)
    private fun requestStoragePermission() {
        val c = context
        if (c != null && EasyPermissions.hasPermissions(c, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            val intent = Intent()
                    .setType("text/plain")
                    .setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(intent, 0)
        } else {
            EasyPermissions.requestPermissions(this, "Necessário acessso ao seu armazenamento para importar os comandos",
                    STORAGE_PERMISSION, Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        val intent = Intent()
                .setType("text/plain")
                .setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(intent, 0)
    }

    interface OnFragmentInteractionListener {
        fun onCancelClick()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        data?.data.let {
            if (it != null) {
                val inputStream = activity?.contentResolver?.openInputStream(it)
                presenter.addMultiplesCommands(inputStream)
            }
        }

    }

    override fun showToast(message: String) {
        onUiThread { toast(message) }
    }

    override fun showAddCommandProgress() {

    }

    override fun hideAddCommandProgress() {

    }

    override fun showToastAndClose(message: String) {
        onUiThread { toast(message) }
        mListener?.onCancelClick()

    }
}
