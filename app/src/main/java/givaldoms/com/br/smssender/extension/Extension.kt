package givaldoms.com.br.smssender.extension

import android.content.Context
import android.content.pm.PackageManager
import android.support.design.widget.TextInputEditText
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.*


/**
 * Created by givaldoms on 11/02/18
 */

fun EditText.clear() {
    this.text = Editable.Factory.getInstance().newEditable("")
}

fun EditText.putText(text: String) {
    this.text = Editable.Factory.getInstance().newEditable(text)
}

fun TextInputEditText.putText(text: String) {
    this.text = Editable.Factory.getInstance().newEditable(text)
}

fun TextInputEditText.disable() {
    this.let {
        isFocusable = false
        isClickable = false
        keyListener = null
    }
}

fun AppCompatActivity.addFragment(fragment: Fragment, containerId: Int,
                                  hasBackStack: Boolean = true, tag: String? = null) {
    val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()

    transaction.add(containerId, fragment, tag)
    if (hasBackStack) {
        transaction.addToBackStack(fragment.id.toString())
    }

    transaction.commit()
}

fun AppCompatActivity.replaceFragment(fragment: Fragment, containerId: Int,
                                      hasBackStack: Boolean = true, tag: String = "") {
    val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()

    transaction.replace(containerId, fragment, tag)
    if (hasBackStack) {
        transaction.addToBackStack(fragment.id.toString())
    }

    transaction.commit()
}

fun Spinner.fill(list: MutableList<String>?, context: Context) {
    if (list == null) return

    if (list.size > 0) list[0] = "Selecione"
    val arrayAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, list)
    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    this.adapter = arrayAdapter
}

fun String.formatToCpf(): String {
    if (this.length > 11 || this.isEmpty()) return ""

    val i0 = this.substring(0, 3)
    val i1 = this.substring(3, 6)
    val i2 = this.substring(6, 9)
    val i3 = this.substring(9, 11)
    return "$i0.$i1.$i2-$i3"

}

fun String.formatToCep(): String {
    if (this.length > 8 || this.isEmpty()) return this

    val i0 = this.substring(0, 5)
    val i1 = this.substring(5, 8)
    return "$i0-$i1"
}

fun AppCompatActivity.hideKeyboard() {
    val v = this.currentFocus
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(v.windowToken, 0)
}

fun AppCompatActivity.hasWriteExternalStoragePermission() =
        ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED


fun AppCompatActivity.requestWriteExternalStoragePermisson(requestCode: Int) {
    ActivityCompat.requestPermissions(this,
            arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), requestCode)
}

fun EditText.addTextChangedListener(event: (s: String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            event(p0?.toString() ?: "")
        }

    })

}

fun AppCompatActivity.showFragment(f: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .show(f)
            .commit()
}


fun AppCompatActivity.hideFragment(f: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .hide(f)
            .commit()
}

fun AppCompatActivity.showFragment(tag: Int) {
    val f0 = this.supportFragmentManager.findFragmentByTag(tag.toString())
    if (f0 != null) {
        this.showFragment(f0)
    }
}

fun AppCompatActivity.hideFragment(tag: Int) {
    val f0 = this.supportFragmentManager.findFragmentByTag(tag.toString())
    if (f0 != null) {
        this.hideFragment(f0)
    }
}