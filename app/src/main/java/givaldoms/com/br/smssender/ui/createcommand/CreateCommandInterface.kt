package givaldoms.com.br.smssender.ui.createcommand

import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.ui.BasePresenter
import givaldoms.com.br.smssender.ui.BaseView
import java.io.InputStream

/**
 * Created by junio on 14/02/2018
 */
interface CreateCommandInterface {

    interface Presenter: BasePresenter<View> {

        fun addCommand(command: Command)

        fun addMultiplesCommands(commands: InputStream?)

    }

    interface View: BaseView {

        fun showAddCommandProgress()

        fun hideAddCommandProgress()

        fun showToastAndClose(message: String)
    }

}