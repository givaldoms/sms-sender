package givaldoms.com.br.smssender.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import givaldoms.com.br.smssender.R
import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.data.User
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.sendSMS
import org.jetbrains.anko.yesButton

/**
 * Created by Givaldo Marques on 21/01/2018
 */

class MessagesAdapter(private val commands: MutableList<Command>,
                      private val user: User,
                      private val mContext: Context) :
        RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MessagesViewHolder(inflater.inflate(R.layout.item_message, parent, false))
    }

    override fun getItemCount(): Int {
        return commands.size
    }

    override fun onBindViewHolder(holder: MessagesViewHolder?, position: Int) {
        val m = commands[position]

        holder?.messageTextView?.text = m.title
        holder?.messageTextView?.setOnClickListener {
            mContext.sendSMS(user.phone, m.generateCommand(user))
        }

    }

    inner class MessagesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val messageTextView: TextView = itemView.findViewById(R.id.messageTextView)
    }

}
