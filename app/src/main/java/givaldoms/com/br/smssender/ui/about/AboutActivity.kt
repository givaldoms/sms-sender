package givaldoms.com.br.smssender.ui.about

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import givaldoms.com.br.smssender.R
import kotlinx.android.synthetic.main.activity_about.*
import org.jetbrains.anko.email

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        val version = "Versão: ${packageManager.getPackageInfo(packageName, 0).versionName}"

        setSupportActionBar(aboutToolbar)

        versionTextView.text = version

        contactButton.setOnClickListener {
            email(email = "junioraqw@gmail.com", subject = "SMS Sender")
        }

        developedBy.setOnClickListener {

        }
    }
}
