package givaldoms.com.br.smssender.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Givaldo Marques on 21/01/2018
 */
@Entity(tableName = "command")
data class Command(
        @ColumnInfo(name = "message")
        var message: String = "",

        @ColumnInfo(name = "title")
        var title: String = "",

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int = 0) {


    @Ignore
    fun generateCommand(user: User) = this.message + user.password
}