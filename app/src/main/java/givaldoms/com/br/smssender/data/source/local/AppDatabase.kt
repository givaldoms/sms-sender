package givaldoms.com.br.smssender.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import givaldoms.com.br.smssender.data.Command


/**
 * Created by givaldoms on 11/02/18
 */

@Database(entities = [(Command::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun CommandDao(): CommandDao

    companion object {
        private val DB_NAME = "smsSender.db"

        @Volatile private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: create(context).also { INSTANCE = it }
                }

        private fun create(context: Context): AppDatabase {
            return Room.databaseBuilder(context.applicationContext,
                    AppDatabase::class.java,
                    DB_NAME).build()
        }

    }




}