package givaldoms.com.br.smssender.data.source.interactor

import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.data.source.local.CommandDao

/**
 * Created by givaldoms on 12/02/18
 */

interface CommandInteractor {

    suspend fun removeAllCommands()

    suspend fun getAllCommands(): List<Command>

    suspend fun addCommand(vararg command: Command)

    suspend fun addCommandList(commandList: List<Command>)

    suspend fun removeCommand(command: Command)

}

class CommandInteractorImpl(private val commandDao: CommandDao) : CommandInteractor {

    override suspend fun getAllCommands():List<Command> {
        return commandDao.getAll()
    }

    override suspend fun addCommand(vararg command: Command) {
        command.forEach {
            commandDao.insertAll(it)
        }
    }

    override suspend fun addCommandList(commandList: List<Command>) {
        commandDao.insertAll(commandList)
    }

    override suspend fun removeAllCommands() {
        commandDao.deleteCommands()
    }

    override suspend fun removeCommand(command: Command) {
        commandDao.removeCommand(command)
    }
}