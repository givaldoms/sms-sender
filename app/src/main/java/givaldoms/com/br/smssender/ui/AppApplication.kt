package givaldoms.com.br.smssender.ui

import android.app.Application
import givaldoms.com.br.smssender.data.source.local.AppDatabase

/**
 * Created by givaldoms on 11/02/18
 */
class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppDatabase.getInstance(this)
    }
}