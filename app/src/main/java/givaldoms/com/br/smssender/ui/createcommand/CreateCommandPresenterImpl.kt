package givaldoms.com.br.smssender.ui.createcommand

import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.data.source.interactor.CommandInteractor
import kotlinx.coroutines.experimental.launch
import java.io.InputStream

class CreateCommandPresenterImpl(private val commandInteractor: CommandInteractor,
                                 private var view: CreateCommandInterface.View?) : CreateCommandInterface.Presenter {

    override fun bindView(view: CreateCommandInterface.View) {
        this.view = view
    }

    override fun unbindView() {
        this.view = null
    }

    override fun addCommand(command: Command) {
        view?.showAddCommandProgress()

        launch {
            commandInteractor.addCommand(command)
            view?.hideAddCommandProgress()
            view?.showToast("Comando adicionado com sucesso")
        }

    }

    override fun addMultiplesCommands(commands: InputStream?) {

        view?.showAddCommandProgress()

        launch {
            val commandsList = validFile(commands)
            if (commandsList != null) {
                commandInteractor.addCommandList(commandsList)
                view?.hideAddCommandProgress()
                view?.showToast("Comandos adicionados com sucesso")

            }
        }
    }

    private suspend fun validFile(commands: InputStream?): List<Command>? {
        if (commands == null) {
            view?.showToast("Erro ao abrir arquivo")
        }

        val commandsList = mutableListOf<Command>()

        commands?.bufferedReader()?.useLines {
            it.forEach {
                val aux = it.split(":")
                if (aux.size != 2) {
                    view?.showToast("Formato de arquivo inválido")
                    return@useLines
                }

                if (aux[0].length < 2 || aux[1].length < 2) {
                    view?.showToast("Formato de arquivo inválido")
                    return@useLines
                }

                commandsList.add(Command(message = aux[1], title = aux[0]))
            }
        }

        if (commandsList.isEmpty()) {
            view?.showToast("Formato de arquivo inválido")
            return null
        }
        return commandsList
    }
}