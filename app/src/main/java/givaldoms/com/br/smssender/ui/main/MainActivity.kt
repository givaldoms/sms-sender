package givaldoms.com.br.smssender.ui.main

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log.d
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import givaldoms.com.br.smssender.R
import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.data.User
import givaldoms.com.br.smssender.data.source.interactor.CommandInteractorImpl
import givaldoms.com.br.smssender.data.source.local.AppDatabase
import givaldoms.com.br.smssender.extension.addFragment
import givaldoms.com.br.smssender.ui.about.AboutActivity
import givaldoms.com.br.smssender.ui.createcommand.CreateCommandFragment
import givaldoms.com.br.smssender.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.yesButton
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt


class MainActivity : AppCompatActivity(),
        CreateCommandFragment.OnFragmentInteractionListener, MainInterface.View {

    private lateinit var mMessageAdapter: MessagesAdapter
    private lateinit var presenter: MainInterface.Presenter
    private var removeItemSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(mainToolbar)
        addFragment(CreateCommandFragment(), createCommandContainer.id)

        presenter = MainPresenterImpl(CommandInteractorImpl(AppDatabase.getInstance(this).CommandDao()),
                User.read(this))

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_exit -> {
                presenter.onSignOutClick()
            }

            R.id.action_about -> startActivity<AboutActivity>()

            R.id.action_clear_all -> {
                alert("Todos os comandos serão apagados", "Continuar com a ação?") {
                    yesButton {
                        presenter.onClearCommandListClick()
                    }
                    cancelButton {  }
                }.show()
            }
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onStart() {
        super.onStart()
        presenter.getCommands()

        addCommandFAB.setOnClickListener {
            enterReveal()
        }

        presenter.bindView(this)

    }

    override fun onStop() {
        super.onStop()
        presenter.unbindView()
    }

    override fun showRemovedItemSnackbar() {
        removeItemSnackbar = longSnackbar(mainContainerCoordinatorLayout, "Comando removido", "Desfazer") {
            presenter.undoDismissItemList()
        }.addCallback(object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                presenter.onSnackbarDismissed()
            }
        })
    }

    private fun addSwipeAdapterEvent() {
        ItemTouchHelper(
                object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                    override fun onMove(recyclerView: RecyclerView,
                                        viewHolder: RecyclerView.ViewHolder,
                                        target: RecyclerView.ViewHolder) = false

                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        removeItemSnackbar?.dismiss()
                        presenter.itemListDismissed(viewHolder.adapterPosition)
                    }
                }
        ).attachToRecyclerView(messagensRecyclerView)
    }

    override fun onBackPressed() {
        if (createCommandContainer.visibility == View.VISIBLE) {
            exitReveal()
            addCommandFAB.show()
            presenter.getCommands()

        } else {
            super.onBackPressed()
        }
    }

    private fun enterReveal() {
        val cx = (addCommandFAB.right + addCommandFAB.left) / 2
        val cy = (addCommandFAB.bottom + addCommandFAB.top) / 2
        val finalRadius = Math.max(createCommandContainer.width, createCommandContainer.height).toFloat()

        val animator = ViewAnimationUtils.createCircularReveal(
                createCommandContainer,
                cx,
                cy,
                0f,
                finalRadius)

        createCommandContainer.visibility = View.VISIBLE
        animator.start()

    }

    private fun exitReveal() {
        val cx = (addCommandFAB.right + addCommandFAB.left) / 2
        val cy = (addCommandFAB.bottom + addCommandFAB.top) / 2
        val startRadius = createCommandContainer.height.toFloat()

        val animator = ViewAnimationUtils.createCircularReveal(
                createCommandContainer,
                cx,
                cy,
                startRadius,
                0f)


        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                createCommandContainer.visibility = View.GONE
            }
        })

        animator.start()

    }

    override fun onCancelClick() {
        exitReveal()
        addCommandFAB.show()
    }

    override fun bindAdapter(commandList: List<Command>, user: User) {
        mMessageAdapter = MessagesAdapter(commandList as MutableList<Command>, user, this)
        messagensRecyclerView.adapter = mMessageAdapter
        messagensRecyclerView.layoutManager = LinearLayoutManager(this)
        addSwipeAdapterEvent()


    }

    override fun attCommandsList() {
        runOnUiThread {
            mMessageAdapter.notifyDataSetChanged()
        }
    }

    override fun onItemRemoved(position: Int) {
        messagensRecyclerView.postDelayed({
            runOnUiThread {
                mMessageAdapter.notifyItemRemoved(position)
            }
        }, 500)
    }


    override fun onItemAdded(position: Int) {
        runOnUiThread {
            mMessageAdapter.notifyItemInserted(position)
        }
    }

    override fun startLoginActivity() {
        runOnUiThread {
            startActivity<LoginActivity>()
            finishAffinity()
        }
    }

    override fun showPromptOnFAB() {
        runOnUiThread {
            MaterialTapTargetPrompt.Builder(this)
                    .setTarget(addCommandFAB)
                    .setPrimaryText("Adicione uma nova mensagem para começar")
                    .show()
        }
    }
}
