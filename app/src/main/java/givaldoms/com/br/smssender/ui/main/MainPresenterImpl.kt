package givaldoms.com.br.smssender.ui.main

import android.util.Log.d
import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.data.User
import givaldoms.com.br.smssender.data.source.interactor.CommandInteractor
import kotlinx.coroutines.experimental.launch

class MainPresenterImpl(private val commandInteractor: CommandInteractor,
                        private val user: User) : MainInterface.Presenter {

    private var view: MainInterface.View? = null
    private val mCommandsList = mutableListOf<Command>()
    private var isFirstTime = true

    private var dismissedItem = Pair(0, Command())


    override fun bindView(view: MainInterface.View) {
        this.view = view
        this.view?.bindAdapter(mCommandsList, user)

        if (user.isEmpty()) {
            this.view?.startLoginActivity()
        }

    }

    override fun unbindView() {
        this.view = null
    }

    override fun getCommands() {
        launch {
            val commands = commandInteractor.getAllCommands()
            commands.filterTo(mCommandsList, { !mCommandsList.contains(it) })
            view?.attCommandsList()

            if (commands.isEmpty() && isFirstTime) {
                view?.showPromptOnFAB()
                isFirstTime = false
            }

        }
    }

    override fun itemListDismissed(positionOnAdapter: Int) {
        dismissedItem = positionOnAdapter to mCommandsList.removeAt(positionOnAdapter)
        view?.onItemRemoved(positionOnAdapter)
        view?.showRemovedItemSnackbar()
    }

    override fun undoDismissItemList() {
        mCommandsList.add(dismissedItem.first, dismissedItem.second)
        view?.onItemAdded(dismissedItem.first)
    }

    override fun onSnackbarDismissed() {
        launch {
            commandInteractor.removeCommand(dismissedItem.second)
        }
    }

    override fun onSignOutClick() {
        launch {
            view?.startLoginActivity()
        }
    }

    override fun onClearCommandListClick() {
        launch {
            commandInteractor.removeAllCommands()
            mCommandsList.clear()
            view?.attCommandsList()
        }
    }
}