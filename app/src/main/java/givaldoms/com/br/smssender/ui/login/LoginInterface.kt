package givaldoms.com.br.smssender.ui.login

import givaldoms.com.br.smssender.ui.BasePresenter
import givaldoms.com.br.smssender.ui.BaseView

/**
 * Created by givaldoms on 13/02/2018
 */
interface LoginInterface {

    interface Presenter: BasePresenter<View> {

        fun onPhoneTextChanged(phone: String)

        fun onPasswordTextChanged(password: String)

    }

    interface View: BaseView {

        fun showPhoneError(errorMessage: String?)

        fun showPasswordError(errorMessage: String?)

        fun setLoginButtonEnabled(e: Boolean)
    }

}