package givaldoms.com.br.smssender.ui.login

class LoginPresenterImpl(private var loginView: LoginInterface.View?) : LoginInterface.Presenter {

    private var phoneStatus = false
    private var paswordStatus = false

    override fun bindView(view: LoginInterface.View) {
        this.loginView = view
    }

    override fun unbindView() {
        this.loginView = null
    }

    override fun onPhoneTextChanged(phone: String) {
        when {
            phone.isBlank() -> {
                loginView?.showPhoneError("Campo obrigatório")
                loginView?.setLoginButtonEnabled(false)
                phoneStatus = false
            }
            phone.length < "## # ####-####".length -> {
                loginView?.showPhoneError("Telefone inválido. Ex: 99 9 9999-9999")
                loginView?.setLoginButtonEnabled(false)
                phoneStatus = false
            }
            else -> {
                loginView?.showPhoneError(null)
                phoneStatus = true

            }
        }

        loginView?.setLoginButtonEnabled(phoneStatus && paswordStatus)

    }

    override fun onPasswordTextChanged(password: String) {
        when {
            password.isBlank() -> {
                loginView?.showPasswordError("Campo obrigatório")
                loginView?.setLoginButtonEnabled(false)
                paswordStatus = false
            }
            password.length < 6 -> {
                loginView?.showPasswordError("Senha deve ter pelo menos 6 caracteres")
                loginView?.setLoginButtonEnabled(false)
                paswordStatus = false
            }
            else -> {
                loginView?.showPasswordError(null)
                paswordStatus = true
            }

        }
        loginView?.setLoginButtonEnabled(phoneStatus && paswordStatus)

    }

}