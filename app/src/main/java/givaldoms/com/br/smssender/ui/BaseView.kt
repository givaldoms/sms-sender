package givaldoms.com.br.smssender.ui

/**
 * Created by givaldoms on 13/02/2018
 */
interface BaseView {

    fun showToast(message: String)

}