package givaldoms.com.br.smssender.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.content.Context
import com.google.gson.Gson

/**
 * Created by Givaldo Marques on 21/01/2018
 */

@Entity(tableName = "user")
data class User(
        @PrimaryKey
        @ColumnInfo(name = "phone")
        val phone: String = "",

        @ColumnInfo(name = "password")
        val password: String = "") {


    companion object {

        private const val KEY = "givaldoms.com.br.smssender.user_key"

        fun read(context: Context): User {
            val preferences = context.getSharedPreferences("givaldoms.com.br.smssender", Context.MODE_PRIVATE)
            val user = preferences.getString(KEY, "")

            return if (user == "") {
                User()
            } else {
                Gson().fromJson(user, User::class.java)
            }

        }
    }

    override fun toString(): String {
        return Gson().toJson(this)
    }


    @Ignore
    fun save(context: Context) {
        val preferences = context.getSharedPreferences("givaldoms.com.br.smssender", Context.MODE_PRIVATE)
        preferences.edit().putString(KEY, this.toString()).apply()
    }

    @Ignore
    fun removeFromPreferences(context: Context) {
        val preferences = context.getSharedPreferences("givaldoms.com.br.smssender", Context.MODE_PRIVATE)
        preferences.edit().clear().apply()
    }

    @Ignore
    fun isEmpty() = this.password.isEmpty() || this.phone.isEmpty()

}