package givaldoms.com.br.smssender.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import givaldoms.com.br.smssender.data.Command

/**
 * Created by givaldoms on 11/02/18
 */

@Dao
abstract class CommandDao {

    @Query("SELECT * FROM command")
    abstract fun getAll(): List<Command>

    @Query("DELETE FROM Command")
    abstract fun deleteCommands()

    @Delete
    abstract fun removeCommand(command: Command)

    @Insert
    abstract fun insertAll(vararg users: Command)

    @Insert
    abstract fun insertAll(commandList: List<Command>)

}