package givaldoms.com.br.smssender.ui.main

import givaldoms.com.br.smssender.data.Command
import givaldoms.com.br.smssender.data.User
import givaldoms.com.br.smssender.data.source.local.CommandDao

/**
 * Created by givaldoms on 12/02/18
 */

interface MainInterface {

    interface Presenter {

        fun bindView(view: View)

        fun unbindView()

        fun getCommands()

        fun itemListDismissed(positionOnAdapter: Int)

        fun undoDismissItemList()

        fun onSnackbarDismissed()

        fun onSignOutClick()

        fun onClearCommandListClick()

    }

    interface View {

        fun bindAdapter(commandList: List<Command>, user: User)

        fun onItemRemoved(position: Int)

        fun onItemAdded(position: Int)

        fun attCommandsList()

        fun startLoginActivity()

        fun showRemovedItemSnackbar()

        fun showPromptOnFAB()

    }

}