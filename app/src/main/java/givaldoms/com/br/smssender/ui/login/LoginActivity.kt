package givaldoms.com.br.smssender.ui.login

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import givaldoms.com.br.smssender.R
import givaldoms.com.br.smssender.data.User
import givaldoms.com.br.smssender.extension.addTextChangedListener
import givaldoms.com.br.smssender.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity(), LoginInterface.View {

    private lateinit var loginPresenter: LoginInterface.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginPresenter = LoginPresenterImpl(this)
    }

    override fun onStart() {
        super.onStart()

        val phone = phoneInputEditText.text.toString()
        val password = passwordInputEditText.text.toString()
        loginPresenter.onPhoneTextChanged(phone)
        loginPresenter.onPhoneTextChanged(password)

        passwordInputEditText.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)
                    && loginButton.isEnabled) {
                loginButton.performClick()
                return@OnKeyListener true
            }

            false
        })

        phoneInputEditText.addTextChangedListener {
            loginPresenter.onPhoneTextChanged(it)
        }

        passwordInputEditText.addTextChangedListener {
            loginPresenter.onPasswordTextChanged(it)
        }

        loginButton.setOnClickListener {
            val p0 = phoneInputEditText.text.toString()
            val p1 = passwordInputEditText.text.toString()
            User(p0.trim(), p1.trim()).save(this)

            startActivity<MainActivity>()
        }
    }

    override fun showToast(message: String) {
        toast(message)
    }

    override fun showPhoneError(errorMessage: String?) {
        phoneInputLayout.error = errorMessage
    }

    override fun showPasswordError(errorMessage: String?) {
        passwordInputLayout.error = errorMessage
    }

    override fun setLoginButtonEnabled(e: Boolean) {
        loginButton.isEnabled = e
    }

}
