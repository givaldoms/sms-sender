package givaldoms.com.br.smssender.ui

/**
 * Created by junio on 13/02/2018
 */
interface BasePresenter<in T> {

    fun bindView(view: T)

    fun unbindView()
}